# Desafio Provi - Data Scientist:

## Análise Inicial:

### Criação de Variáveis:
**installmentValueOverDeclaredMonthlyEarns:** razão entre as variáveis *installmentValue* e *declaredMonthlyEarns*  
**serasaIncomeCategories:** conversão das faixas de renda para tipo categoria  
**dayOfInstallmentDueDate :** extração do dia da variável *installmentDueDate*  
**dayOfDueDateCategories:** conversão da variável *dayOfInstallmentDueDate*  para tipo categoria  
**monthOfInstallmentDueDate :** extração do mês da variável *installmentDueDate*  
**monthOfInstallmentDueDate :** conversão da variável *monthOfInstallmentDueDate*  para tipo categoria  
**installmentDueDate:** conversão da variável *installmentDueDate* para data  
**averagePh3aIncome:** média entre limite inferior e superior da variável *ph3aIncome *  
**installmenValueOverAveragePh3aIncome :** razão entre as variáveis *installmentValue* e *averagePh3aIncome*  
**booleanInstallmentStatus:** conversão da variável *installmentStatus*  para boolean  
**declaredMonthlyEarnsAndAveragePh3aMean:** média entre as variáveis *declaredMonthlyEarns* e *averagePh3aMean*  
**declaredMonthlyEarnsAndAveragePh3aDifference:** diferença entre as variáveis *declaredMonthlyEarns* e *averagePh3aMean*  
**declaredMonthlyEarnsAndAveragePh3aAbsoluteDifference:** diferença absoluta entre as variáveis *declaredMonthlyEarns* e *averagePh3aMean* 


### Cálculo com correlação das variáveis numéricas:
Fazendo a correlação destacam-se  
		
		| declaredMonthlyEarnsAndAveragePh3aDifference              | ~0.22 |
		| installmentValue                                          | ~0.20 |
		| installmenValueOverAveragePh3aIncome                      | ~0.12 |

## Análise Detalhada:

### Análise de variáveis derivadas de ph3aIncome:
Nota-se que muitos campos de *ph3aIncome* possuem valor null  
Removendo estes valores, temos que há aproximadamente 50 entradas com valores válidos *ph3aIncome*  
Fazendo a correlação temos:  
		
		| declaredMonthlyEarnsAndAveragePh3aDifference              | ~0.22 |
		| declaredMonthlyEarnsAndAveragePh3aMean                    | ~0.17 |
		| installmenValueOverAveragePh3aIncome                      | ~0.12 |

### Análise da variável serasaIncomeCategories:
Nota-se que há muitos casos de categoria -1 (derivada de None) como visto em 'Plot 1'  
Removendo estas colunas restam apenas 17 entradas e temos a distribuição de 'Plot 2'  
Fazendo a correlação (Cramer's V) temos: ~0.32  

### Análise da variável *dayOfInstallmentDueDate* :
Vemos pela crosstab que há quase o dobro de pagantes no dia 10 (64) do que para o dia 25 (37), visível na figura 'Plot 3'  
Fazendo a correlação (Cramer's V) temos: 0.18184516272770587   

### Análise da variável *monthOfInstallmentDueDate* :
Vemos pela crosstab que há uma distribuição de maioria pagantes ou não pagantes a cada mês como visto em 'Plot 4'
Fazendo a correlação (Cramer's V) temos: 0.93

## Feature Selection - Extra Tree Classifier:
Vemos pelo 'Plot 5' que a variável de maior significância dentre as selecionadas é *monthOfDueDateCategories*  seguido de *serasaIncomeCategories*  
Mantendo apenas os valores *averagePh3aIncome* válidos a partir do dataframe original, vemos em 'Plot 6' que a variável mais significativa segundo este método, é *monthOfDueDateCategories* seguido de *serasaIncomeCategories*  
Mantendo apenas os valores *serasaIncome* válidos a partir do dataframe original, vemos em 'Plot 7' que a variável mais significativa segundo este método, é *monthOfDueDateCategories* seguido de *averagePh3aIncome*  
Analisando as variáveis de valores numéricos, vemos em 'Plot 8' temos *installmenValueOverAveragePh3aIncome* com variável mais significativa, seguida de *declaredMonthlyEarnsAndAveragePh3aDifference*  
Utilizando variáveis que não possuem valor null temos como variável mais significativa *monthOfDueDateCategories* seguida de *dayOfDueDateCategories* como visto em 'Plot 9'  
Para variáveis categóricas temos *monthOfDueDateCategories* como mais significativa como visto em 'Plot 10'  

## Considerações:
A variável *id* tem um índice de correlação de 0.637380 mas é uma base enviesada, pois os pagantes estão no final do dataframe, logo, para um valor de *id* crescente haverá uma correlação  
A variável *serasaIncomeCategories* é um bom preditor dado que a ausência desta informação está correlacionada ao pagamento, cabe verificação se a informação tem algum significado real ou é apenas resultado de um dataset enviesado 

## Conclusões:
* Para variáveis categóricas, utilizando a correlação Cramer's V a variável que melhor explica é *monthInstallmentDueDate*   
* Para variáveis numéricas, utilizando a correlação de Pearson (função .corr()) a variável que melhor explica é *declaredMonthlyEarnsAndAveragePh3aDifference*  
* Utilizando Extra Tree Classifier para feature selection para variáveis selecionadas, a variável mais significativa é *monthInstallmentDueDate* seguida de *serasaIncomeCategories*  
* Utilizando Extra Tree Classifier para feature selection com valores válidos para *ph3aIncome* (51 entradas, 'Plot 6') a variável mais significativa é *monthInstallmentDueDate* seguida de *serasaIncomeCategories*  
* Utilizando Extra Tree Classifier para feature selection com valores válidos para *serasaIncome* (17 entradas, 'Plot 7') a variável mais significativa é *monthInstallmentDueDate* seguida de *averagePh3aIncome*  
* Utilizando Extra Tree Classifier para feature selection apenas com variáveis numéricas, a variável mais significativa é *installmentValueOverPh3aIncome*  
* Utilizando Extra Tree Classifier para feature selection apenas variáveis catégóricas, a variável mais significativa é *monthInstallmentDueDate* seguida de *averagePh3aIncome*  

## Melhorias Possíveis para o Código:
* Separar funções para arquivo 
* Gerar testes para as funções
* Criar função para Feature Selection

## Melhorias Possíveis para a Análise:
* Uso de outros métodos de correlação e Feature Selection
* Estimativa de significância dado o tamanho da amostra
* Estudo do método de cálculo de *serasaIncome* e entendimento do significado da ausência desta informação; o mesmo para ph3aIncome

## Aprendizado Desenvolvido:
* Biblioteca Pandas
* IDE Jupyter Notebook
* Funções básicas de colunas
* Manipulação de dataframes
* Uso de variáveis categóricas
* Feature Selection
* Correlação e correlação categórica
* Geração de gráficos